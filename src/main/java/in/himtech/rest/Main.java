package in.himtech.rest;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;

import in.himtech.rest.demo.EntityNotFoundException;
import in.himtech.rest.demo.SecondServices;
import in.himtech.rest.demo.ThirdServices;
import in.himtech.rest.entity.EmployeeEntityReader;
import in.himtech.rest.entity.EntityTest;


public class Main {
	public static void main(String[] args) {
		URI baseUri = UriBuilder.fromUri("http://localhost/").port(8090).build();
		ResourceConfig config = new ResourceConfig();
		config.register(EntityTest.class);
		config.register(SecondServices.class);
		config.register(ThirdServices.class);
		config.register(EntityNotFoundException.class);
		config.register(EmployeeEntityReader.class);
		config.property(ServerProperties.METAINF_SERVICES_LOOKUP_DISABLE, true);
		JdkHttpServerFactory.createHttpServer(baseUri, config);		
	}
}
