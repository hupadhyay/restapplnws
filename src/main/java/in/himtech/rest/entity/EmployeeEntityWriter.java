package in.himtech.rest.entity;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

@Provider
public class EmployeeEntityWriter  implements MessageBodyWriter<Employee> {

	@Override
	public long getSize(Employee arg0, Class<?> arg1, Type arg2, Annotation[] arg3, MediaType arg4) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isWriteable(Class<?> arg0, Type type, Annotation[] arg2, MediaType mType) {
		// TODO Auto-generated method stub
		return (type == Employee.class && mType.toString().equals(MediaType.APPLICATION_JSON));
	}

	@Override
	public void writeTo(Employee emp, Class<?> arg1, Type arg2, Annotation[] arg3, MediaType arg4,
			MultivaluedMap<String, Object> arg5, OutputStream os) throws IOException, WebApplicationException {
		JsonWriter writer = Json.createWriter(os);
		
//		writer.
		
		JsonObjectBuilder joBuilder = Json.createObjectBuilder();
		joBuilder.add("id", emp.getId());
		joBuilder.add("name", emp.getName());
		joBuilder.add("city", emp.getCity());
		
		JsonObject jsonObject = joBuilder.build();
		
		writer.writeObject(jsonObject);
		
		writer.close();
	}

}
