package in.himtech.rest.entity;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("entity")
public class EntityTest {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Employee getEmpJSON(){
		return new Employee("234", "Emp_Json", "Delhi");
	}	
	
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public Employee getEmpXML( ){
		return new Employee("34523", "Emp_Json", "Hyderabad");
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public String addEmpJSON(Employee emp){
		return "received Emp data : " + emp;
	}
	
	
	@POST
	@Consumes(MediaType.APPLICATION_XML)
	public String addEmpXML(Employee emp){
		return "received Emp data : " + emp;
	}

}
