package in.himtech.rest.entity;

import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.Provider;

@Provider
public class EmployeeEntityReader implements MessageBodyReader<Employee> {

	@Override
	public boolean isReadable(Class<?> arg0, Type type, Annotation[] arg2, MediaType arg3) {
		// TODO Auto-generated method stub
		return (type == Employee.class);
	}

	@Override
	public Employee readFrom(Class<Employee> emp, Type type, Annotation[] arg2, MediaType arg3,
			MultivaluedMap<String, String> string, InputStream is) throws IOException, WebApplicationException {
		
		JsonReader reader = Json.createReader(is);
		JsonObject jsonEmployee= reader.readObject();
		
		String id = jsonEmployee.getString("id");
		String name = jsonEmployee.getString("name");
		String city = jsonEmployee.getString("name");
		
		Employee employee = new Employee();
		employee.setId(id);
		employee.setName(name + "from custome provider");
		employee.setCity(city);
	
		return employee;
	}

}
