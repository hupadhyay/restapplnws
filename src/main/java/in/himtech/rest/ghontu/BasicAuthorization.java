package in.himtech.rest.ghontu;

import java.io.IOException;
import java.util.Base64;
import java.util.StringTokenizer;

public class BasicAuthorization {

	public boolean authenticate(String strAuth){
		if(strAuth == null){
			return false;
		} else {
			// header value format will be "Basic encodedstring" for Basic
			// authentication. Example "Basic YWRtaW46YWRtaW4="
			String encodedUserPwd = strAuth.replaceFirst("Basic" + " ", "");
			String userAndPwd = null;
			try{
				byte[] decodedBytes = Base64.getDecoder().decode(encodedUserPwd);
				userAndPwd = new String(decodedBytes, "UTF-8");
			}catch(IOException exp){
				exp.printStackTrace();
			}
			StringTokenizer st = new StringTokenizer(userAndPwd, ":");
			String userName = st.nextToken();
			String password = st.nextToken();
			
			if(userName.equals("Himanshu") && password.equals("himpc")){
				System.out.println("Authorized User.");
				return true;
			} else {
				return false;
			}
		}
	}
}
