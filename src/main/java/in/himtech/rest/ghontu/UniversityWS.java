package in.himtech.rest.ghontu;

import java.io.File;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

@Path("university")
public class UniversityWS {

	@GET
	@Produces(MediaType.TEXT_HTML)
	public String getHtmlContent(){
		return "Hey, I am HTML content";
	}
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String gettextContent(){
		return "Hey, I am Plain text content";
	}
	
	@PUT
	@Produces(MediaType.TEXT_PLAIN)
	public String updateContent(){
		return "Content updated.";
	}
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("{rollNo1}/{rollNo2}")
	public String getStudentInfo(@PathParam("rollNo1") String roll1, @PathParam("rollNo2")String roll2){
		StringBuffer sb = new StringBuffer();
		sb.append("Student Information:\n");
		sb.append("Roll Number1: " + roll1 + "\n");
		sb.append("Roll Number2: " + roll2);
	
		return sb.toString();
	}
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("query")
	public String getStudentQueryInfo(@QueryParam("qprm1") String str1, @QueryParam("qprm2") String str2){
		StringBuffer sb = new StringBuffer();
		sb.append("Query Param Details:\n");
		sb.append("Query Param 1: " + str1 + "\n");
		sb.append("Query Param 2: " + str2);
	
		return sb.toString();
	}
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("matrix")
	public String getStudentMatrixInfo(@MatrixParam("mprm1") String str1, @MatrixParam("mprm2") String str2){
		StringBuffer sb = new StringBuffer();
		sb.append("Matrix Param Details:\n");
		sb.append("Matrix Param 1: " + str1 + "\n");
		sb.append("Matrix Param 2: " + str2);
	
		return sb.toString();
	}
	
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Path("form")
	public String getStudentFormInfo(@FormParam("name") String str1, @FormParam("age") String str2){
		StringBuffer sb = new StringBuffer();
		sb.append("Student detail submitted:\n");
		sb.append("Name of Student: " + str1 + "\n");
		sb.append("Age of Student: " + str2);
	
		return sb.toString();
	}
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("getrawfile")
	public File getFileInfo(){
		File file = new File("C:\\Himanshu\\Hello.txt");
		
		return file;
	}
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("getfile")
	public Response getFileResponseBuilderInfo(){
		File file = new File("C:\\Himanshu\\Hello.txt");
		ResponseBuilder responseBuilder = Response.ok((Object)file);
		responseBuilder.header("Content-Disposition", "attachment;filename=helloText.txt");
		return responseBuilder.build();
	}
	
	@GET
	@Produces("application/pdf")
	@Path("getrawpdf")
	public File getPdfInfo(){
		File file = new File("C:\\Himanshu\\ccs.pdf");
		
		return file;
	}
	
	@GET
	@Produces("application/pdf")
	@Path("getpdf")
	public Response getPdfFile(){
		File file = new File("C:\\Himanshu\\ccs.pdf");
		ResponseBuilder responseBuilder = Response.ok((Object)file);
		responseBuilder.header("Content-Disposition", "attachment;filename=CreditCard.pdf");
		return responseBuilder.build();
	}
	
	@GET
	@Produces("image/jpg")
	@Path("getrawimg")
	public File getImgInfo(){
		File file = new File("C:\\Himanshu\\anushka.jpg");
		
		return file;
	}
	
	@GET
	@Produces("image/jpg")
	@Path("getimg")
	public Response getImgFile(){
		File file = new File("C:\\Himanshu\\anushka.jpg");
		ResponseBuilder responseBuilder = Response.ok((Object)file);
		responseBuilder.header("Content-Disposition", "attachment;filename=family.jpg");
		return responseBuilder.build();
	}
}
