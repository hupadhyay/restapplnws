package in.himtech.rest.ghontu;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AuthenticationFilter implements Filter{

	public final String AUTHENTICATION_HEADER = "Authorization";
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain filterChain) throws IOException, ServletException {
		if(request instanceof HttpServletRequest){
			HttpServletRequest httpServletRequest = (HttpServletRequest)request;
			String authenticatValue = httpServletRequest.getHeader(AUTHENTICATION_HEADER);
			
			BasicAuthorization basicAuthorization = new BasicAuthorization();
			boolean isAuthenticated = basicAuthorization.authenticate(authenticatValue);
			
			if(isAuthenticated){
				filterChain.doFilter(request, response);
			}else{
				if(response instanceof HttpServletResponse){
					HttpServletResponse httpServletResponse = (HttpServletResponse)response;
					httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
					
				}
			}
		}
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
