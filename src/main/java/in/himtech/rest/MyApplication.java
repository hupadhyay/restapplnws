package in.himtech.rest;



import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.glassfish.jersey.server.ResourceConfig;

import in.himtech.rest.entity.EntityTest;

//@ApplicationPath("/rest")
//public class MyApplication extends Application{
//    @Override
//    public Set<Class<?>> getClasses() {
//        Set<Class<?>> classSet = new HashSet<Class<?>>();
//        classSet.add(in.himtech.rest.demo.FirstServices.class);
//        classSet.add(in.himtech.rest.demo.SecondServices.class);
//        classSet.add(in.himtech.rest.demo.ThirdServices.class);
//        System.out.println("Scanning classes..");
//        return classSet;
//    }
//    
//    @Override
//    public Set<Object> getSingletons() {
//    	// TODO Auto-generated method stub
//    	return super.getSingletons();
//    }   
//}

@ApplicationPath("/rest")
public class MyApplication extends ResourceConfig {
	public MyApplication() {
		packages("in.himtech.rest");
	}
}