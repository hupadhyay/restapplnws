package in.himtech.rest.demo;


import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class EntityNotFoundException implements ExceptionMapper<IllegalArgumentException> {

	@Override
	public Response toResponse(IllegalArgumentException iaExp) {
		// TODO Auto-generated method stub
		System.out.println("Exception called");
		return Response.status(Status.NOT_FOUND).encoding(iaExp.getMessage()).build();
	}

}
