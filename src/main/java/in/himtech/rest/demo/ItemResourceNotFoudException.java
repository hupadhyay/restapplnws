package in.himtech.rest.demo;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

public class ItemResourceNotFoudException extends WebApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3962528803865388362L;

	public ItemResourceNotFoudException() {
		super(Response.status(Status.NOT_FOUND).build());
	}

	public ItemResourceNotFoudException(String message) {
		super(Response.status(Status.NOT_FOUND).entity(message).type(MediaType.TEXT_PLAIN).build());
	}
}
