package in.himtech.rest.demo;

import javax.ws.rs.BeanParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

@Path("second")
public class SecondServices {

	/**
	 * Accessing of query string
	 * (http://himpc.local:8080/RestApplnWS/rest/second/query?name1=Hritika&name2=Hrishik)
	 * 
	 * @param name1
	 * @param name2
	 */
	@GET
	@Path("query")
	public String accessQueryString(@QueryParam("name1") String name1,
			@QueryParam("name2") String name2) {
		System.out.println("QueryParameter Name1: " + name1);
		System.out.println("QueryParameter Name2: " + name2);
		return "Query Parameter Values are: " + name1 + " and " + name2;
	}

	/**
	 * Accessing of matrix parameter
	 * (http://himpc.local:8080/RestApplnWS/rest/second/matrix;name1=Hritika;name2=Hrishik;name3=Anushka)
	 * 
	 * @param name1
	 * @param name2
	 * @return
	 */
	@GET
	@Path("matrix")
	public String accessMatrixString(@MatrixParam("name1") String name1,
			@MatrixParam("name2") String name2, @MatrixParam("name3") String name3) {
		System.out.println("MatrixParameter Name1: " + name1);
		System.out.println("MatrixParameter Name2: " + name2);
		System.out.println("MatrixParameter Name3: " + name3);
		return "Matrix Parameter Values are: " + name1 + " and " + name2 + " and " + name3;
	}
	
	/**
	 * Access through html form with post method
	 * (http://himpc.local:8080/RestApplnWS/rest/second/add)
	 * @param studentName
	 * @param age
	 * @return
	 */
	@POST
	@Path("add")
	public String accessFormValue(@FormParam("name") String studentName, @FormParam("age") String age){
		System.out.println("First Form Value: " + studentName);
		System.out.println("Second Form Value: " + age);
		return "Student record (" + studentName + ", " + age + ") has been added.";
	}
	
	@POST
	public void post(@BeanParam MyBeanParam beanParam, String entity) {
	    String pathParam = beanParam.getPathParam(); 
	    System.out.println(pathParam);
	    String matrixParam = beanParam.getMatrixParam();
	    System.out.println(matrixParam);
	    String queryParam = beanParam.getQueryParam();
	    System.out.println(queryParam);
	    String headerParam = beanParam.getHeaderParam();
	    System.out.println(headerParam);
	}

}
