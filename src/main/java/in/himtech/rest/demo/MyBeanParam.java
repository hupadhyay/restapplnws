package in.himtech.rest.demo;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.Encoded;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

public class MyBeanParam {
	
	@PathParam("p")
	private String pathParam;

	@MatrixParam("m")
	@Encoded
	@DefaultValue("default")
	private String matrixParam;
	
	@HeaderParam("header")
	private String headerParam;

	private String queryParam;

	public MyBeanParam(@QueryParam("q") String queryParam) {
		this.queryParam = queryParam;
	}
	
	public String getPathParam() {
		return pathParam;
	}
	
	public void setPathParam(String pathParam) {
		this.pathParam = pathParam;
	}
	
	public String getMatrixParam() {
		return matrixParam;
	}
	
	public void setMatrixParam(String matrixParam) {
		this.matrixParam = matrixParam;
	}
	
	public String getHeaderParam() {
		return headerParam;
	}
	
	public void setHeaderParam(String headerParam) {
		this.headerParam = headerParam;
	}
	
	public String getQueryParam() {
		return queryParam;
	}
	
	public void setQueryParam(String queryParam) {
		this.queryParam = queryParam;
	}
}
		

