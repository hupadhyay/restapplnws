package in.himtech.rest.demo;

import java.io.File;
import java.io.FileNotFoundException;

import javax.activation.MimetypesFileTypeMap;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
/**
 * this class will show how to handle binary or large resource in rest web service.
 * @author himanshu
 *
 */
@Path("third")
public class ThirdServices {

	@Path("text")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Response getfileService(){
		File file = new File("/Users/himanshu/Desktop/murga.txt");
		ResponseBuilder rb = Response.ok((Object)file);
		rb.header("content-disposition", "attachment;filename=murgamanus.txt");
		return rb.build();
	}
	
	@Path("pdf")
	@GET
	@Produces("applicaiton/pdf")
	public Response getPdfService(){
		File file = new File("/Users/himanshu/Desktop/JavaWSTutorial1.pdf");
		ResponseBuilder rb = Response.ok((Object)file);
		rb.header("content-disposition", "attachment;filename=webServices.pdf");
		return rb.build();
	}
	
	@Path("img")
	@GET
	@Produces("image/jpeg")
	public Response getImageService(){
		File file = new File("/Users/himanshu/Desktop/Nanhe.jpeg");
		ResponseBuilder rb = Response.ok((Object)file);
		rb.header("content-disposition", "attachment;photo_of_him.jpeg");
		return rb.build();
	}
	
	@GET
	@Path("/images/{image}")
	@Produces("image/*")
	public Response getImage(@PathParam("image") String image) {
		File file = new File("/Users/himanshu/Desktop/"+image);
	
		if (!file.exists()) {
//			throw new WebApplicationException(404);
//			throw new ItemResourceNotFoudException();
			throw new IllegalArgumentException();
		}
	
		String mt = new MimetypesFileTypeMap().getContentType(file);
		return Response.ok(file, mt).build();
	}
}
