package in.himtech.rest.demo;

import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

@Path("hello")
public class FirstServices {
	private String greeting = "Good Morning";

	// Uses of @PathParam
	// (http://himpc.local:8080/RestApplnWS/rest/hello/Hritika)
	@GET
	@Path("{name}")
	public String sayHello(@PathParam("name") String name) {
		String returnMessage = name + ", Very " + greeting + " Dear";
		return returnMessage;

	}

	// User of default url (http://himpc.local:8080/RestApplnWS/rest/hello)
	@GET
	public String myGreeting() {
		return greeting + ", Hritika";
	}

	// Updating resource at server end using PUT resource. Have to set
	// method:PUT (http://himpc.local:8080/RestApplnWS/rest/Namaste)
	@PUT
	@Path("{greet}")
	public void updateGreeting(@PathParam("greet") String strMsg) {
		this.greeting = strMsg;
		System.out.println("Message is updated successfully!");
	}

	// Uses of @PathParam with two variable.
	// (http://himpc.local:8080/RestApplnWS/rest/hello/salute/Namaste)
	@GET
	@Path("{msg}/{name}")
	@Produces(MediaType.TEXT_PLAIN)
	public String sayHello(@PathParam("msg") String message,
			@PathParam("name") String name) {
		String returnMessage = name + ", " + message;
		return returnMessage;
	}
	
	
	// (http://himpc.local:8080/RestApplnWS/rest/hello/content)
	@GET
//	@Produces({"application/xml", "application/json"})
	@Path("content")
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public String doGetAsXmlOrJson(@HeaderParam("Content-Type") String contentType) {
	    System.out.println("Content-Type is : " + contentType);
	    return contentType;
	    
	}
	
	
	@GET
	@Path("/uridemo/*")
	@Produces(MediaType.TEXT_PLAIN)
	public String getUriInfo(@Context UriInfo ui) {
	    MultivaluedMap<String, String> queryParams = ui.getQueryParameters();
	    MultivaluedMap<String, String> pathParams = ui.getPathParameters();
	    System.out.println("query Param: " + queryParams);
	    System.out.println("path Param" + pathParams);
		return pathParams.toString();
	}
	
	@GET
	@Path("headers")
	@Produces(MediaType.TEXT_PLAIN)
	public String getHeaders(@Context HttpHeaders hh) {
	    MultivaluedMap<String, String> headerParams = hh.getRequestHeaders();
	    Map<String, Cookie> pathParams = hh.getCookies();
	    System.out.println("header params: " + headerParams);
	    System.out.println("Path params: " + pathParams);
	    return "headerDemo doen";
	}
}
